import TelegramBot from 'node-telegram-bot-api';

export interface IInfoNode {
    buttons: TelegramBot.KeyboardButton[][];
}

export interface ILink {
    link: string;
}

export interface IDNode {
    tags?: string[];
    t: string;
    img?: string;
    buttons: TelegramBot.KeyboardButton[][];
    nodes: {
        [key: string]: IDNode | ILink;
    }
}

export interface IRecurciveDNode extends IDNode {
    parent?: IDNode;
}
