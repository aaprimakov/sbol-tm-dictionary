import { IDNode } from './model/common'
import { getButton as b } from './utils';

export const rootNode: IDNode = {
    t: 'Выберите категорию',
    buttons: [[b('babel')]],
    nodes: {
        babel: {
            t: 'Документация Babel',
            tags: ['babel', 'бабель'],
            img: 'https://miro.medium.com/max/6552/1*XmHUL5DeySv_dGmvbPqdDQ.png',
            buttons: [
                [b('Ссыдка ->')],
                [b('Пресеты плагинов')],
                [
                    b('Настройка ->'),
                    b('Playground ->')
                ],
                [
                    b('Videos ->'),
                    b('Блог ->')
                ],
                [b('github')],
            ],
            nodes: {
                'Пресеты плагинов': {
                    t: 'Выберите пресет',
                    img: 'babel-presets.png',
                    buttons: [
                        [b('env ->')],
                        [b('flow ->')],
                        [b('react ->')],
                        [b('typescript ->')],
                    ],
                    nodes: {
                        'env ->': {
                            link: 'https://babeljs.io/docs/en/babel-preset-env'
                        },
                        'flow ->': {
                            link: 'https://babeljs.io/docs/en/babel-preset-flow'
                        },
                        'react ->': {
                            link: 'https://babeljs.io/docs/en/babel-preset-react'
                        },
                        'typescript ->': {
                            link: 'https://babeljs.io/docs/en/babel-preset-typescript'
                        }
                    }
                },
                'Ссыдка ->': { link: 'https://babeljs.io/docs/en/' },
                'Настройка ->': { link: 'https://babeljs.io/setup' },
                'Playground ->': { link: 'https://babeljs.io/repl' },
                'Videos ->': { link: 'https://babeljs.io/videos' },
                'Блог ->': { link: 'https://babeljs.io/blog/' },
                'github': {
                    t: 'Репозиторий babel',
                    tags: ['git', 'babel'],
                    buttons: [
                        [b('Ссыдка ->')],
                        [
                            b('Спонсоры ->'),
                            b('faq ->')
                        ],
                    ],
                    nodes: {
                        'Ссыдка ->': { link: 'https://github.com/babel/babel' },
                        'Спонсоры ->': { link: 'https://github.com/babel/babel#sponsors' },
                        'faq ->': { link: 'https://github.com/babel/babel#faq' },
                    }
                },
            }
        }
    }
}
