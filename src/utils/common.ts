import TelegramBot from 'node-telegram-bot-api';

import { IDNode, ILink, IRecurciveDNode } from '../model/common'

export const getButton = (text): TelegramBot.KeyboardButton => ({ text });

export const addParentNode = (node: IRecurciveDNode | ILink, parent = null): IRecurciveDNode => {
    if ((node as IRecurciveDNode).nodes) {
        const nodes = Object.keys((node as IRecurciveDNode).nodes)
            .reduce(
                (prev: any, n: string) => ({
                    ...prev,
                    [n]: addParentNode((node as IRecurciveDNode).nodes[n], node)
                }),
                {}
            );

        (node as IRecurciveDNode).nodes = nodes;
    }

    (node as IRecurciveDNode).parent = parent;

    return (node as IRecurciveDNode)
}

const getTags = (node: IDNode) => {
    const tags = [node.t, ...(node.tags || [])].map(tag => ({ tag, node }))

    let moretags = []
    if(node.nodes) {
        Object.keys(node.nodes).map(key => {
            moretags = [ ...moretags, ...getTags(node.nodes[key] as IDNode)]
        })
    }

    return [ ...tags, ...moretags.filter(i => !!i.tag) ]
}

export const indexNodes = (rootNode: IDNode) => {
    return getTags(rootNode)
}
