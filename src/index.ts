import { config } from 'dotenv'
import TelegramBot from 'node-telegram-bot-api';
import fs from 'fs'
import path from 'path'

import { IDNode, ILink, IRecurciveDNode } from './model/common'
import { getUserIdFromMessage, getUserDataFromMessage, addParentNode, indexNodes, getButton as b } from './utils';
import { rootNode } from './dictionary'

config()

const bot = new TelegramBot(process.env.SBOL_DICTIOARY_BOT_TOKEN, { polling: true });

const MAP = addParentNode(rootNode)
const searchArrray = indexNodes(MAP)

// console.log(searchArrray)

const getKeyboarOptions = (buttons: TelegramBot.KeyboardButton[][], addBack = true): TelegramBot.SendBasicOptions => ({
    reply_markup: {
        keyboard: [ ...buttons, (addBack ? [ { text: '<- Назад' }, { text: '<- В начало' } ] : []) ]
    }
})

const data = new Map<number, IRecurciveDNode>();
const operate = (userId: number, node: IRecurciveDNode) => {
    if (node.img) { 
        if (/^http/i.test(node.img)) {
            bot.sendPhoto(userId, node.img)
        } else {
            bot.sendPhoto(userId, fs.readFileSync(path.resolve(__dirname, '../images', node.img)))
        }
    }
    bot.sendMessage(userId, node.t, getKeyboarOptions(node.buttons, !!node.parent))
}

bot.on('message', (msg: TelegramBot.Message) => {
    const text = msg.text
    if (!/\/start/.test(text)) {
        const userId = getUserIdFromMessage(msg);

        if (!data.has(userId)) {
            data.set(userId, MAP)
            operate(userId, MAP)
            return
        }

        const currentNode = data.get(userId)
        
        if (text === '<- Назад') {
            const parentNode = currentNode.parent
            data.set(userId, parentNode)
            operate(userId, parentNode)
            return
        }
        
        if (text === '<- В начало') {
            data.set(userId, MAP)
            operate(userId, MAP)
            return
        }

        const nextNode = currentNode.nodes[text]

        if (!nextNode) {
            try {
                const queryResult = searchArrray
                    .filter(tagObject => (tagObject.tag || '')
                    .includes(msg.text))

                const filtered = queryResult.filter((aa, i) => queryResult.map(aaa => aaa.node.t).indexOf(aa.node.t) === i)
                const node: IRecurciveDNode = {
                    parent: MAP,
                    t: 'Результаты поиска',
                    buttons: [filtered.map(r => b(r.node.t) )],
                    nodes: filtered.reduce((acc, q) => {
                        acc[q.node.t] = q.node
                        return acc
                    }, {})

                }
                data.set(userId, node)
                operate(userId, node)
            } catch (error) {
                console.error(error)
            }
            return
        }
        
        if ((nextNode as ILink).link) {
            bot.sendMessage(userId, (nextNode as ILink).link, getKeyboarOptions(currentNode.buttons))
            return
        }

        data.set(userId, (nextNode as IDNode))
        operate(userId, (nextNode as IDNode))
    }
})

bot.onText(/\/start/, async (msg: TelegramBot.Message) => {
    const userId = getUserIdFromMessage(msg);
    const user = getUserDataFromMessage(msg);
    data.set(userId, MAP)

    bot.sendMessage(userId, `
${user.first_name || user.username}, вас приветствует бот помогающий сориентироваться в нашем информационном простаранстве
Тут вы можете найти ссылки на любые интересующие вас темы касающиеся работы.
Приятного пользования!!
  `,
        getKeyboarOptions(MAP.buttons, false)
    );
});
